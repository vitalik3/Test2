﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace DebugPanel
{
    public class SelectWidgetByToggleGroup : MonoBehaviour
    {
        [SerializeField] private Toggle[] _toggles;

        private void ToggleValueChanged(Toggle toggle)
        {
            var widget = toggle.GetComponent<IToggleWidget>();

            if (toggle.isOn)
            {
                widget.Show();
            }
            else
            {
                widget.Hide();
            }
        }

        private void Start()
        {
            var widgets = FindObjectsOfType<MonoBehaviour>().OfType<IPanelWidget>();

            foreach (var widget in widgets)
            {
                foreach (var toggle in _toggles)
                {
                    var toggleWidget = toggle.GetComponent<IToggleWidget>();
                    if (toggleWidget != null && toggleWidget.WidgetType == widget.WidgetType)
                    {
                        toggle.isOn = true;
                    }
                }
            }

            foreach (var toggle in _toggles)
            {
                toggle.onValueChanged.AddListener(delegate { ToggleValueChanged(toggle); });
            }
        }

        private void OnDestroy()
        {
            foreach (var toggle in _toggles)
            {
                toggle.onValueChanged.RemoveListener(delegate { ToggleValueChanged(toggle); });
            }
        }
    }
}
