using UnityEngine;

namespace DebugPanel
{
    public class EnableDebugPanelWidget : MonoBehaviour, IToggleWidget
    {
        [SerializeField] private WidgetType _widgetType;
        [SerializeField] private GameObject _panel;
        private GameObject _widget;

        public WidgetType WidgetType => _widgetType;

        private void OnEnable()
        {
            var widget = FindObjectOfType<PanelWidget>();
            if (widget != null && widget.WidgetType == _widgetType)
            {
                _widget = widget.gameObject;
            }
        }

        public void Show()
        {
            if (_panel != null && _widget == null)
            {
                _widget = Instantiate(_panel);
            }
        }

        public void Hide()
        {
            if (_widget != null)
            {
                Destroy(_widget);
            }
        }
    }
}
