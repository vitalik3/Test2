﻿using SRDebugger;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DebugPanel
{
    public class PanelWidget : MonoBehaviour, IPanelWidget, IDragHandler, IEndDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        [SerializeField] private WidgetType _widgetType;
        [SerializeField] private RectTransform _panel;
        [SerializeField] private Vector3 _defaultPosition;
        [SerializeField] private DefaultTabs _tab = DefaultTabs.GameEvents;
        private bool _isDrag;

        public WidgetType WidgetType => _widgetType;

        public void OnDrag(PointerEventData eventData)
        {
            var pos = _panel.position;
            pos.x = eventData.position.x;
            pos.y = eventData.position.y;
            _panel.position = pos;
            _isDrag = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!_isDrag)
            {
                SRDebug.Instance.ShowDebugPanel(_tab);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _isDrag = false;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            SavePosition();
        }

        private void ResetPosition()
        {
            _panel.anchoredPosition = _defaultPosition;
        }

        private void OnEnable()
        {
            _panel.SetAsFirstSibling();
            LoadPosition();
        }

        private void LoadPosition()
        {
            if (PlayerPrefs.HasKey("DebugWidgetPositionX"))
            {
                Vector2 pos = new Vector3(PlayerPrefs.GetFloat("DebugWidgetPositionX"), PlayerPrefs.GetFloat("DebugWidgetPositionY"), 0);
                _panel.anchoredPosition = pos;
            }
            else
            {
                ResetPosition();
            }
        }

        private void SavePosition()
        {
            PlayerPrefs.SetFloat("DebugWidgetPositionX", _panel.anchoredPosition.x);
            PlayerPrefs.SetFloat("DebugWidgetPositionY", _panel.anchoredPosition.y);
        }
    }
}
