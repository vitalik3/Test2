﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace DebugPanel
{
    public class FPSCounter : MonoBehaviour
    {
        [SerializeField] private int framesBufferShort = 5;
        [SerializeField] private int updateEvery = 3;
        [SerializeField] private Text counter;
        private string formattedString = "FPS: {value}";
        private int _updatesLeft;
        private Queue<float> framesShort = new Queue<float>();

        private void Update()
        {
            framesShort.Enqueue(Time.unscaledDeltaTime);
            while (framesShort.Count > framesBufferShort)
                framesShort.Dequeue();

            if (_updatesLeft < 1)
            {
                var avgShort = framesShort.Count() / framesShort.Sum();
                counter.text = formattedString.Replace("{value}", $"{avgShort:##}");
                _updatesLeft = updateEvery;
            }

            _updatesLeft--;
        }
    }
}
