﻿using UnityEngine;
using UnityEngine.UI;

namespace DebugPanel
{
    public class LogNotification : MonoBehaviour
    {
        [SerializeField] private Text _warning, _error;
        private int _warningCounter, _errorCounter;

        private void OnEnable()
        {
            ResetLogCount();
            Application.logMessageReceived += HandleLog;
        }

        private void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            switch (type)
            {
                case LogType.Warning:
                    _warningCounter += 1;
                    break;
                case LogType.Assert:
                    _errorCounter += 1;
                    break;
                case LogType.Error:
                    _errorCounter += 1;
                    break;
                case LogType.Exception:
                    _errorCounter += 1;
                    break;
            }

            UpdateWidgetCounter();
        }

        private void ResetLogCount()
        {
            _warningCounter = 0;
            _errorCounter = 0;
        }

        private void UpdateWidgetCounter()
        {
            _warning.text = _warningCounter.ToString();
            _error.text = _errorCounter.ToString();
        }
    }
}
