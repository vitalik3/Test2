﻿using SRF;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;

namespace DebugPanel
{
    public class UsedMemory : MonoBehaviour
    {
        [SerializeField] private Text _currentUsedMemory;

        private void Update()
        {
            var current = Profiler.GetTotalAllocatedMemoryLong();
            var currentMb = (current >> 10);
            currentMb /= 1024;

            if (currentMb > 0)
            {
                _currentUsedMemory.text = "<color=#FFFFFF>{0}</color>MB".Fmt(currentMb);
            }
        }
    }
}
