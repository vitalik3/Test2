﻿using System;
using System.Collections;
using UnityEngine;

namespace DebugPanel
{
    public class CleanMemoryButton : MonoBehaviour
    {
        public void CleanMemory()
        {
            StartCoroutine(GCCollect());
        }

        private IEnumerator GCCollect()
        {
            yield return Resources.UnloadUnusedAssets();
            GC.Collect();
        }
    }
}
