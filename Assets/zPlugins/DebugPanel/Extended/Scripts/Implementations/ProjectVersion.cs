﻿using UnityEngine;
using UnityEngine.UI;

namespace DebugPanel
{
    public class ProjectVersion : MonoBehaviour
    {
        [SerializeField] private Text _version;

        private void Start()
        {
            _version.text = "Build:\n"+Application.version;
        }
    }
}
