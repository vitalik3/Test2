using System;
using UnityEngine;

namespace DebugPanel
{
    public class SafeArea : MonoBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        private Rect _rect;
        private bool _checkSafeArea;
        private DeviceOrientation _deviceOrientation;

        private void OnEnable()
        {
            if (_rectTransform == null)
            {
                _rectTransform = GetComponent<RectTransform>();
            }

            var safeArea = Screen.safeArea;

            if (safeArea != _rect)
            {
                ApplySafeArea(safeArea);
                _checkSafeArea = true;
            }
        }

        private void ApplySafeArea(Rect area)
        {
            var anchorMin = area.position;
            var anchorMax = area.position + area.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            _rectTransform.anchorMin = anchorMin;
            _rectTransform.anchorMax = anchorMax;
            _rect = area;
            _deviceOrientation = Input.deviceOrientation;
        }

        private void Update()
        {
            if (!_checkSafeArea)
            {
                return;
            }

            if (Input.deviceOrientation != _deviceOrientation)
            {
                var safeArea = Screen.safeArea;
                ApplySafeArea(safeArea);
            }
        }
    }
}
