﻿using UnityEngine;
using UnityEngine.Diagnostics;

namespace DebugPanel
{
    public class ForceCrashButton : MonoBehaviour
    {
        public void OnClick()
        {
            Utils.ForceCrash(ForcedCrashCategory.FatalError);
        }
    }
}
