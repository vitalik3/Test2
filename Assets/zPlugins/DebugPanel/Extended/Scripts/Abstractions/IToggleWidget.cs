namespace DebugPanel
{
    public interface IToggleWidget
    {
        void Show();
        void Hide();
        WidgetType WidgetType { get; }
    }
}
