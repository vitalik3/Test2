namespace DebugPanel
{
    public interface IPanelWidget
    {
        WidgetType WidgetType { get; }
    }
}
