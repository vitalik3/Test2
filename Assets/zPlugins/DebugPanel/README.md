Debug Panel
Для исключения ресурсов дебаг панели в релизной сборке, папки  DebugPanel/Extended и DebugPanel/StompyRobot удаляются на момент сборки.
 
Custom Events:
1. Для создания дополнительных разделов в дебаг панели, необходимо сделать дубликат префаба 
DebugPanel/StompyRobot/SRDebugger/Resources/SRDebugger/UI/Prefabs/Tabs/CustomEvents

2. В настройках компонента OptionsTabController "AllowedCategories" добавить разрешенные категории для этого раздела.
(partial class SROptions, CategoryAttribute)
