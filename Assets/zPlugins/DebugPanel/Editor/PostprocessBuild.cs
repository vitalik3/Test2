﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Callbacks;
using UnityEngine;

public class PostprocessBuild : MonoBehaviour, IPreprocessBuild
{
    private static Dictionary<string, string> _resourcesFolder = new Dictionary<string, string>();
    public int callbackOrder { get; }

    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        var buildPlayerOptions = GetBuildPlayerOptions();

        if ((buildPlayerOptions.options & BuildOptions.Development) != 0)
        {
            Debug.Log("DEVELOPMENT BUILD");
            _resourcesFolder.Clear();
        }
        else
        {
            Debug.Log("RELEASE BUILD");

            _resourcesFolder = new Dictionary<string, string>
            {
                {Application.dataPath + "/" + "DebugPanel/StompyRobot", Application.dataPath + "/" + "DebugPanel/StompyRobot~"},
                {Application.dataPath + "/" + "DebugPanel/Extended", Application.dataPath + "/" + "DebugPanel/Extended~"}
            };

            foreach (var dir in _resourcesFolder)
            {
                if (IsDirectoryExists(dir.Key))
                {
                    Directory.Move(dir.Key, dir.Value);
                    Debug.Log("Move " + dir.Key + " to " + dir.Value);
                }
            }

            AssetDatabase.Refresh();
        }
    }

    [PostProcessBuild]
    private static void OnPostprocessBuild(BuildTarget target, string pathToBuildProject)
    {
        foreach (var dir in _resourcesFolder)
        {
            if (IsDirectoryExists(dir.Value))
            {
                Directory.Move(dir.Value, dir.Key);
                Debug.Log("Move " + dir.Value + " to " + dir.Key);
            }
        }

        AssetDatabase.Refresh();
    }

    private static bool IsDirectoryExists(string dir)
    {
        return Directory.Exists(Path.Combine(dir));
    }

    private static BuildPlayerOptions GetBuildPlayerOptions(bool askForLocation = false, BuildPlayerOptions defaultOptions = new BuildPlayerOptions())
    {
        MethodInfo method = typeof(BuildPlayerWindow.DefaultBuildMethods).GetMethod("GetBuildPlayerOptionsInternal", BindingFlags.NonPublic | BindingFlags.Static);
        return (BuildPlayerOptions) method.Invoke(null, new object[] {askForLocation, defaultOptions});
    }
}
