﻿namespace DebugPanel {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using UnityEngine;
#if USE_INAPPS
    using UnityEngine.Purchasing;
    using Random = UnityEngine.Random;
#endif
    using UnityEngine.UI;

    public class PurchasesInfoController : MonoBehaviour {
        [SerializeField] private int FontSize;
        [SerializeField] private Text Text;
        [SerializeField] private Button ClearHistoryButton;
        [SerializeField] private ScrollRect ContentScrollRect;

        private const string RECEIPTS_PREFS_KEY = "dp_receipts";
        private const string NO_UNITY_IAP_ISTALLED_TEXT = "No Unity IAP installed. If installed, add USE_INAPPS define.";
        private const string NO_PURCHASES_SAVED = "No purchases were saved in history";
        private const string RECEIPTS_VISUAL_DELIMITER = "\n\n";

        private StringBuilder _consoleTextStringBuilder;

        private int _receiptsPrintedCount;

        [Serializable]
        private class ReceiptsData {
            public List<string>  AllReceipts;
        }

        private ReceiptsData _cache;
        
#if USE_INAPPS
        private void Awake() {
            _consoleTextStringBuilder = new StringBuilder();

            var savedReceiptsJSON = PlayerPrefs.GetString(RECEIPTS_PREFS_KEY, String.Empty);
            _cache = JsonUtility.FromJson<ReceiptsData>(savedReceiptsJSON) ??
                              new ReceiptsData() { AllReceipts = new List<string>() };
            UpdateText();
        }

        private void OnEnable() {
            Text.fontSize = FontSize;
            ClearHistoryButton.onClick.AddListener(OnClearHistoryButtonClick);
        }

        private void OnDisable() {
            ClearHistoryButton.onClick.RemoveListener(OnClearHistoryButtonClick);
        }
        
        private void UpdateText() {
            if (_cache.AllReceipts.Count == 0) {
                _receiptsPrintedCount = 0;
                _consoleTextStringBuilder.Clear();
                Text.text = NO_PURCHASES_SAVED;
                return;
            }

            if (_receiptsPrintedCount == _cache.AllReceipts.Count) {
                return;
            }

            var receiptsCountToPrint = _cache.AllReceipts.Count - _receiptsPrintedCount;
            
            for (int i = _cache.AllReceipts.Count - receiptsCountToPrint; i < _cache.AllReceipts.Count; i++) {
                _consoleTextStringBuilder.Append(_cache.AllReceipts[i]);
                _consoleTextStringBuilder.Append(RECEIPTS_VISUAL_DELIMITER);
            }

            _receiptsPrintedCount = _cache.AllReceipts.Count;
            Text.text = _consoleTextStringBuilder.ToString();

            // not very correct, but fine tho
            ContentScrollRect.normalizedPosition = Vector2.down;
        }
        
        private void OnClearHistoryButtonClick() {
            PlayerPrefs.DeleteKey(RECEIPTS_PREFS_KEY);
            _cache.AllReceipts.Clear();
            UpdateText();
        }

        private void HandleReceiptData(string data) {
            _cache.AllReceipts.Add(data);

            var cachedReceiptsJSON = JsonUtility.ToJson(_cache);
            PlayerPrefs.SetString(RECEIPTS_PREFS_KEY, cachedReceiptsJSON);
            UpdateText();
        }

        /// <summary>
        /// Shold be called whed IStoreListener completes purchase or when IStoreController confirms pending purchase
        /// </summary>
        /// <param name="product"></param>
        public void OnPurchaseConfirmed(Product product) {
            HandleReceiptData(product.receipt);
        }
        
#if TEST_CONSOLE_INAPPS_LOGGING
        public void AddFakePurchase() {
            var randomNumber = Random.value * 1000;
            HandleReceiptData($"Gotcha! \nThis is completely fake receitp. No purchase was made. \nBut you can still donate your money to somalian pirates. \nWe need still need {randomNumber} $");
        }
        
        void OnGUI() {
            if (GUI.Button(new Rect(Screen.width - 10 - 150, Screen.height - 10 - 100, 150, 100), "Fake Purchase")) {
                AddFakePurchase();
            }
        }
#endif
#else
        private void OnEnable() {
            Text.text = NO_UNITY_IAP_ISTALLED_TEXT;
            Text.fontSize = FontSize;
        }
#endif
    }
}