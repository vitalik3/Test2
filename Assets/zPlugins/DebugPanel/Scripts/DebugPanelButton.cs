﻿using UnityEngine;

namespace DebugPanel
{
    public class DebugPanelButton : MonoBehaviour
    {
        [SerializeField] private GameObject _button;

        public void ShowDebugPanel()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            SRDebug.Instance.ShowDebugPanel();
#endif
        }

        private void Start()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            SRDebug.Init();
            _button.SetActive(true);
#else
                if (_button.activeSelf)
                {
                    _button.SetActive(false);
                }
#endif
        }
    }
}
