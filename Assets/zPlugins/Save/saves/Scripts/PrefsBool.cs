﻿using System;
using UnityEngine;

namespace MyUtilites.Saves
{
     public class PrefsBool : MonoBehaviour
     {
          private readonly string _key;
          private bool _valueCached;

          public string Key => _key;
          public event Action<bool> OnChanged;

          public PrefsBool( string key )
          {
               _key = key;
               Value = PlayerPrefs.GetInt( key ) == 0 ? false : true;
          }

          public PrefsBool( string key, bool valueIfNone )
          {
               _key = key;
               if ( !PlayerPrefs.HasKey( _key ) )
               {
                    Value = valueIfNone;
               } else
               {
                    Value = PlayerPrefs.GetInt( key ) == 0 ? false : true;
               }
          }

          public bool Value
          {
               get => _valueCached;
               set {
                    var valPrev = _valueCached;
                    _valueCached = value;
                    if ( valPrev != value )
                    {
                         PlayerPrefs.SetInt( _key, value ? 1 : 0 );
                         PlayerPrefs.Save( );
                         OnChanged?.Invoke( value );
                    }
               }
          }
     }
}


