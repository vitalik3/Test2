using System;
using UnityEngine;

namespace MyUtilites.Saves
{
    public class PrefsInt
    {
        private readonly string _key;
        private int _valueCached;

        public string Key => _key;
        public event Action<int> OnChanged;

        public PrefsInt(string key)
        {
            _key = key;
            Value = PlayerPrefs.GetInt(key);
        }

        public PrefsInt(string key, int valueIfNone)
        {
            _key = key;
            if (!PlayerPrefs.HasKey(this._key))
            {
                Value = valueIfNone;
            }
            else
            {
                Value = PlayerPrefs.GetInt(key);
            }
        }

        public int Value
        {
            get => _valueCached;
            set
            {
                var valPrev = _valueCached;
                _valueCached = value;
                if (valPrev != value)
                {
                    PlayerPrefs.SetInt(_key, value);
                    PlayerPrefs.Save();
                    OnChanged?.Invoke(value);
                }
            }
        }

        public void Add(int value)
        {
            Value = _valueCached + value;
        }
    }
}
