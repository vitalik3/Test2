using System;
using UnityEngine;

namespace MyUtilites.Saves
{
    public class PrefsFloat
    {
        private readonly string _key;
        private float _valueCached;

        public string Key => _key;
        public event Action<float> OnChanged;

        public PrefsFloat(string key)
        {
            _key = key;
            Value = PlayerPrefs.GetFloat(key);
        }

        public PrefsFloat(string key, float valueIfNone)
        {
            _key = key;
            if (!PlayerPrefs.HasKey(_key))
            {
                Value = valueIfNone;
            }
            else
            {
                Value = PlayerPrefs.GetFloat(key);
            }
        }

        public float Value
        {
            get => _valueCached;
            set
            {
                var valPrev = _valueCached;
                _valueCached = value;
                if (valPrev != value)
                {
                    PlayerPrefs.SetFloat(_key, value);
                    PlayerPrefs.Save();
                    OnChanged?.Invoke(value);
                }
            }
        }

        public void Add(float value)
        {
            Value = _valueCached + value;
        }
    }
}
