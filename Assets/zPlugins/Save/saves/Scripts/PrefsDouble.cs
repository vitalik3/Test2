using System;
using UnityEngine;

namespace MyUtilites.Saves
{
    public class PrefsDouble
    {
        private readonly string _key;
        private double _valueCached;

        public string Key => _key;
        public event Action<double> OnChanged;

        public PrefsDouble(string key)
        {
            _key = key;
            if (double.TryParse(PlayerPrefs.GetString(key), out double v))
            {
                Value = v;
            }
        }

        public PrefsDouble(string key, double valueIfNone)
        {
            _key = key;
            if (!PlayerPrefs.HasKey(_key))
            {
                Value = valueIfNone;
            }
            else if (double.TryParse(PlayerPrefs.GetString(key), out double v))
            {
                Value = v;
            }
        }

        public double Value
        {
            get => _valueCached;
            set
            {
                _valueCached = value;
                PlayerPrefs.SetString(_key, value.ToString());
                PlayerPrefs.Save();
                OnChanged?.Invoke(value);
            }
        }

        public void Add(double value)
        {
            Value = _valueCached + value;
        }
    }
}
