using System;
using UnityEngine;

namespace MyUtilites.Saves
{
    public class PrefsIntArray
    {
        private readonly string _key;
        private int[] valueCached;

        public string Key => _key;
        public event Action<int[]> OnChanged;

        public PrefsIntArray(string key)
        {
            _key = key;

            if (PlayerPrefs.HasKey(key))
            {
                var json = PlayerPrefs.GetString(key);
                var wrap = JsonUtility.FromJson<ArrayWrapper>(json);
                valueCached = wrap.Array;
            }
            else
            {
                valueCached = new int[0];
            }
        }

        public int[] Value
        {
            get
            {
                if (valueCached == null)
                {
                    valueCached = new int[0];
                }

                return valueCached;
            }
            set
            {
                if (value == null)
                {
                    value = new int[0];
                }

                valueCached = value;
                var str = JsonUtility.ToJson(new ArrayWrapper {Array = value});
                PlayerPrefs.SetString(_key, JsonUtility.ToJson(value));
                PlayerPrefs.Save();
                OnChanged?.Invoke(value);
            }
        }

        private class ArrayWrapper
        {
            public int[] Array;
        }
    }
}
