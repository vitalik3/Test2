using System;
using UnityEngine;

namespace MyUtilites.Saves
{
    public class PrefsString
    {
        private readonly string _key;
        private string _valueCached;

        public string Key => _key;
        public event Action<string> OnChanged;

        public PrefsString(string key)
        {
            _key = key;
            Value = PlayerPrefs.GetString(key);
        }

        public string Value
        {
            get => _valueCached;
            set
            {
                var valPrev = _valueCached;
                _valueCached = value;
                if (!string.Equals(valPrev, value))
                {
                    PlayerPrefs.SetString(_key, value);
                    PlayerPrefs.Save();
                    OnChanged?.Invoke(value);
                }
            }
        }
    }
}
