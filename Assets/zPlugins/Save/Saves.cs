using UnityEngine;

namespace MyUtilites.Saves
{
    public static partial class Saves
    {
        public static PrefsInt PrivacyPolicy { get; } = new PrefsInt("PrivacyPolicy");
        public static PrefsInt SoundSetting { get; } = new PrefsInt("SoundSetting", 1);
        public static PrefsInt VibrationSetting { get; } = new PrefsInt("VibrationSetting", 1);
        public static PrefsInt SoftCurrency { get; } = new PrefsInt("SoftCurrency", 0);
        public static PrefsInt HardCurrency { get; } = new PrefsInt("HardCurrency", 0);
        public static PrefsInt CurrentLevel { get; } = new PrefsInt("CurrentLevel", 0);
        public static PrefsInt CurrentLevelRunPath { get; } = new PrefsInt("CurrentLevelRunPath", 0); 
        public static PrefsInt ChestKeys { get; } = new PrefsInt("ChestKeys", 0);
        public static PrefsString ApplicationVersion { get; } = new PrefsString("CurrentApplicationVersion");
        public static PrefsInt SelectPalitraTexture { get; } = new PrefsInt("SelectPalitraTexture");

        public static bool IsFirstApplicationStart()
        {
            if (!PlayerPrefs.HasKey("FirstApplicationStart"))
            {
                PlayerPrefs.SetInt("FirstApplicationStart", 1);
                return true;
            }

            return false;
        }
    }
}
