using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace MyUtilites.Saves
{
    public static class Serializer
    {
        public static T Load<T>(string filename) where T : class
        {
            var path = Path.Combine(Application.persistentDataPath + "/" + filename + ".dat");

            if (File.Exists(path))
            {
                try
                {
                    using (Stream stream = File.OpenRead(path))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        return formatter.Deserialize(stream) as T;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                }
            }

            return default(T);
        }

        public static void Save<T>(string filename, T data) where T : class
        {
            using (Stream stream = File.OpenWrite(Path.Combine(Application.persistentDataPath + "/" + filename + ".dat")))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, data);
            }
        }

        public static void Delete(string filename)
        {
            try
            {
                File.Delete(Path.Combine(Application.persistentDataPath + "/" + filename + ".dat"));
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        public static bool IsFileExist(string filename)
        {
            var path = Path.Combine(Application.persistentDataPath + "/" + filename + ".dat");

            if (File.Exists(path))
            {
                return true;
            }

            return false;
        }
    }
}
