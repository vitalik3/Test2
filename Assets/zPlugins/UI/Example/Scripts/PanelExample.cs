﻿using System.Collections;
using Modules.UI;
using UnityEngine;

namespace UIExample
{
    public class PanelExample : MonoBehaviour, IPanel
    {
        [SerializeField] private EnumExample _id;

        public int Id => (int) _id;

        public IEnumerator Show()
        {
            gameObject.SetActive(true);
            yield break;
        }

        public IEnumerator Hide()
        {
            gameObject.SetActive(false);
            yield break;
        }
    }
}
