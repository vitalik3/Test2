using Modules.UI;
using UnityEngine;

namespace UIExample
{
    public class OpenPanelButton : MonoBehaviour
    {
        [SerializeField] private EnumExample _id;

        public void OpenPanel()
        {
            UI.Instance.Open((int) _id);
        }
    }
}
