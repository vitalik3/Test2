namespace Modules.UI
{
    public interface IDataProvider
    {
        T GetData<T>();
    }
}
