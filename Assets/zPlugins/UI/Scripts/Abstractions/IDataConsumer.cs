namespace Modules.UI
{
    public interface IDataConsumer
    {
        void SetData(IDataProvider dataProvider);
    }
}
