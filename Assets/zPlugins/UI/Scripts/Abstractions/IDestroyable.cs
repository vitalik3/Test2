namespace Modules.UI
{
    public interface IDestroyable
    {
        void Destroy();
    }
}
