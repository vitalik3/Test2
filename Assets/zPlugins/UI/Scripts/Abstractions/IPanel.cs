using System.Collections;

namespace Modules.UI
{
    public interface IPanel
    {
        int Id { get; }
        IEnumerator Show();
        IEnumerator Hide();
        T GetComponent<T>();
    }
}
