﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;

namespace Modules.UI
{
    [RequireComponent(typeof(Canvas))]
    public class UI : MonoBehaviour
    {
        [SerializeField] private int _initPanelId;
        [SerializeField] private GameObject[] _canvasPanels;
        [SerializeField] private GameObject[] _prefabPanels;
        private Canvas _canvas;
        private Stack<IPanel> _stack;
        private List<IPanel> _panels;

        public event Action<int> OnPanelChanged;

        public static UI Instance = null;

        public void Open(int id, IDataProvider dataProvider = null)
        {
            OpenPanel(id, dataProvider);
        }

        public void Back()
        {
            var current = CurrentPanelId;
            if (current != null)
            {
                OpenPanel(_stack.First(p => p.Id != current).Id);
            }
        }

        public int CurrentPanelId
        {
            get
            {
                IPanel currentPanel = _stack.Peek();
                Assert.IsTrue(currentPanel != null, "Current panel is null");
                return currentPanel.Id;
            }
        }

        public void CreateSinglePanel(int id, IDataProvider dataProvider = null)
        {
            var panel = InstantiatePanel(id);
            if (dataProvider != null)
            {
                var dataConsumer = panel.GetComponent<IDataConsumer>();
                dataConsumer?.SetData(dataProvider);
            }

            StartCoroutine(panel.Show());
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance == this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
            Initialize();
        }

        private void Initialize()
        {
            _canvas = GetComponent<Canvas>();
            _stack = new Stack<IPanel>();
            _panels = new List<IPanel>();

            foreach (var canvasPanel in _canvasPanels)
            {
                IPanel panel = canvasPanel.GetComponent<IPanel>();
                if (panel != null)
                {
                    _panels.Add(panel);
                }
            }

            Open(_initPanelId);
        }

        private IEnumerator HidePanel(IPanel panel)
        {
            var objectPanel = panel.GetComponent<Transform>().gameObject;
            if (objectPanel.activeSelf)
            {
                var destroyable = panel.GetComponent<IDestroyable>();
                if (destroyable != null)
                {
                    _panels.Remove(panel);
                    yield return StartCoroutine(panel.Hide());
                    destroyable.Destroy();

                    if (objectPanel != null)
                    {
                        Destroy(objectPanel);
                    }
                }
                else
                {
                    yield return StartCoroutine(panel.Hide());
                }
            }
        }

        private IPanel FindPanel(int id)
        {
            IPanel panel = null;

            while (_stack.Count > 0)
            {
                var peek = _stack.Peek();
                if (peek.Id == id)
                {
                    panel = peek;
                    _stack.Pop();
                    break;
                }

                _stack.Pop();
            }

            return panel;
        }

        private void OpenPanel(int id, IDataProvider dataProvider = null)
        {
            if (_stack.Count > 0)
            {
                var panel = _stack.Peek();
                if (panel.Id != id)
                {
                    StartCoroutine(HidePanel(panel));
                }
            }

            IPanel nextPanel = null;
            nextPanel = _stack.Any(c => c.Id == id) ? FindPanel(id) : _panels.FirstOrDefault(c => c.Id == id);

            if (nextPanel != null)
            {
                _stack.Push(nextPanel);
                SetPanelActive(_stack.Peek());

                if (dataProvider != null)
                {
                    var dataConsumer = nextPanel.GetComponent<IDataConsumer>();
                    dataConsumer?.SetData(dataProvider);
                }

                StartCoroutine(nextPanel.Show());
            }
            else
            {
                var dynamicPanel = InstantiatePanel(id);

                if (dynamicPanel != null)
                {
                    if (_stack.Count > 0)
                    {
                        var panel = _stack.Peek();
                        if (panel.Id != id)
                        {
                            StartCoroutine(HidePanel(panel));
                        }
                    }

                    _panels.Add(dynamicPanel);
                    _stack.Push(dynamicPanel);
                    SetPanelActive(_stack.Peek());

                    if (dataProvider != null)
                    {
                        var dataConsumer = dynamicPanel.GetComponent<IDataConsumer>();
                        dataConsumer?.SetData(dataProvider);
                    }

                    StartCoroutine(dynamicPanel.Show());
                }
            }

            OnPanelChanged?.Invoke(id);
        }

        private void SetPanelActive(IPanel panel)
        {
            var obj = panel.GetComponent<Transform>();
            if (!obj.gameObject.activeSelf)
            {
                obj.gameObject.SetActive(true);
            }
        }

        private IPanel InstantiatePanel(int id)
        {
            foreach (var prefabPanel in _prefabPanels)
            {
                var panelComponent = prefabPanel.GetComponent<IPanel>();
                if (panelComponent != null && panelComponent.Id == id)
                {
                    return Instantiate((Object) panelComponent, _canvas.transform, false) as IPanel;
                }
            }

            return null;
        }

        [ContextMenu("Open")]
        public void DebugOpen()
        {
            Open(_initPanelId);
        }
    }
}
