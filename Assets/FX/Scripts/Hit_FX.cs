using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit_FX : MonoBehaviour
{
    public Transform emmiter;
    public Animator anim;
    private Camera cam;
    private string tag= "Hit_ConcreteDestroy";
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
 
    }

    public void Hit()
    {
        GameObject obj = ObjectPooler.instance.SpawnFromPool(tag, emmiter.position, emmiter.rotation, emmiter);

        obj.transform.LookAt(cam.transform.position);
    }

    public void End()
    {
        anim.SetTrigger("Idle");
    }
    void Update()
    {

            if (Input.GetKeyDown(KeyCode.A))
            {
            tag = "Hit_ConcreteDestroy";
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            tag = "Hit_Concrete";
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            tag = "Hit_Bronze";
        }

        if (Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("Hit");
        }
    
    }
}
