using UnityEngine;

public class Land : MonoBehaviour
{
    [SerializeField] private GameObject _view;
    
    private void Start()
    {
        _view.SetActive(true);
    }
}
