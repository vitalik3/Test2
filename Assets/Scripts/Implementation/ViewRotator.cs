using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using MyUtilites;
using UnityEngine;

public class ViewRotator : MonoBehaviour
{
   [SerializeField] private float _speedRotate;
   [SerializeField] private Transform _pointAnchorRotate;
   [SerializeField] private Transform _pointFollow;
   [SerializeField] private float _safeZoneCameraOffsetY;
   [SerializeField] private Vector3 _overOffset;
   [SerializeField] private float _overForceOffsetY;

   private CinemachineVirtualCamera _virtualCameraSelf;
   private CinemachineTransposer _transposer;
   private Vector3 _currentOffset;
   private Vector3 _cash;
   private float _startMagnitude;

   private void Start()
   {
      _virtualCameraSelf = GetComponent<CinemachineVirtualCamera>();
      _transposer = _virtualCameraSelf.GetCinemachineComponent<CinemachineTransposer>();
      _virtualCameraSelf.LookAt = _pointAnchorRotate;

      _startMagnitude = (_pointAnchorRotate.position - _pointFollow.position).magnitude;
      _currentOffset = _overOffset;
   }

   private void Update()
   {
      var targetPosition = _pointAnchorRotate.position - _pointFollow.position + Vector3.ClampMagnitude(_pointFollow.position + _currentOffset - _pointAnchorRotate.position, _startMagnitude);
      _transposer.m_FollowOffset = Vector3.SmoothDamp(_transposer.m_FollowOffset, targetPosition, ref _cash, .5f);
      _pointFollow.RotateAround(_pointAnchorRotate.position, Vector3.up, _speedRotate);
   }
   

   private void OnPickSuccess(object raycastHit)
   {
      var hit = (RaycastHit) raycastHit;
      if (hit.point.y > _pointAnchorRotate.position.y + _safeZoneCameraOffsetY)
      {
         var yOffset = (hit.point.y - _pointAnchorRotate.position.y) * _overForceOffsetY;
         _currentOffset = new Vector3(0, yOffset, 0) + _overOffset;
      } else if (hit.point.y < _pointAnchorRotate.position.y - _safeZoneCameraOffsetY)
      {
         var yOffset = (hit.point.y - _pointAnchorRotate.position.y) * _overForceOffsetY;
         _currentOffset = new Vector3(0, yOffset, 0) + _overOffset;
      }
   }

   private void OnEnable()
   {
      EventManager.StartListening(GameEventType.SUCCESS_PICK_HIT, OnPickSuccess);
   }

   private void OnDisable()
   {
      EventManager.StopListening(GameEventType.SUCCESS_PICK_HIT, OnPickSuccess);
   }
}