using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using DG.Tweening;
using MyUtilites;
using RayFire;
using UnityEngine;
using Random = UnityEngine.Random;

public class Pick : MonoBehaviour
{
   [SerializeField] private float _force;
   [SerializeField] private float _radiusSphere;
   [SerializeField] private float _sense;
   [SerializeField] private Transform _centerMonolit;
   [SerializeField] private Transform _renderContainer;
   [SerializeField] private Vector3 _bounds;
   [SerializeField] private float _pickTimeout;

   private Coroutine _cor;
   private float _lastPickDelay = 0;
   private Vector3 _currentBounds;
   private Vector3 _pointStartRaycast;

   private bool Randombool => Random.Range(0, 1f) < .5f;

   private void Start()
   {
      UpdateSelfPosition();
   }

   private void Update()
   {
      if (Input.GetMouseButtonDown(0))
      {
         ClearCoroutine();
         _cor = StartCoroutine(HandlerDrag());
      } else if (!Input.GetMouseButton(0))
      {
         UpdateSelfPosition();
      }
   }

   private IEnumerator HandlerDrag()
   {
      var wait = new WaitForFixedUpdate();
      var lastTouch = Input.mousePosition;
      while (Input.GetMouseButton(0))
      {
         var drag = (Input.mousePosition - lastTouch) * _sense;

         if (drag.y != 0 && drag.y > 0 && _currentBounds.y + drag.y < _bounds.y || drag.y < 0 && _currentBounds.y + drag.y > -_bounds.y)
         {
            _centerMonolit.localEulerAngles = new Vector3(
               _centerMonolit.localEulerAngles.x + drag.y,
               _centerMonolit.localEulerAngles.y,
               _centerMonolit.localEulerAngles.z);
            _currentBounds.y += drag.y;
         }

         if (drag.x != 0 && drag.x > 0 && _currentBounds.x + drag.x < _bounds.x || drag.x < 0 && _currentBounds.x + drag.x > -_bounds.x)
         {
            _centerMonolit.localEulerAngles = new Vector3(
               _centerMonolit.localEulerAngles.x,
               _centerMonolit.localEulerAngles.y - drag.x,
               _centerMonolit.localEulerAngles.z);
            _currentBounds.x += drag.x;
         }

         UpdateSelfPosition();
         RayCast();
         
         lastTouch = Input.mousePosition;

         yield return wait;
      }

      _cor = null;
   }

   private void UpdateSelfPosition()
   {
      _pointStartRaycast = _centerMonolit.position - _centerMonolit.forward * 2f;
      transform.position = _pointStartRaycast + _centerMonolit.forward;
      UpdateAngleRender();
   }

   private void UpdateAngleRender()
   {
      var dot = Vector3.Dot(_centerMonolit.position - transform.position, Vector3.up);
      if (dot > 0)
      {
         var projectale = Vector3.ProjectOnPlane(_renderContainer.forward, Vector3.up);
         _renderContainer.LookAt(_renderContainer.position + projectale);
      } else
      {
         _renderContainer.LookAt(_centerMonolit);
      }
   }

   private bool RayCast()
   {
      var ray = new Ray(transform.position, _centerMonolit.position - transform.position);

      if (Physics.Raycast(ray, out RaycastHit hit, 2f - _radiusSphere, LayerMask.GetMask(GameTags.PICK_MASK, GameTags.STONE_PICKED)))
      {
         if (_lastPickDelay <= 0)
         {
            DoPick(ray, hit);
            _lastPickDelay = _pickTimeout;
         }

         transform.LookAt(_centerMonolit);
         _lastPickDelay -= Time.deltaTime;

         _renderContainer.position = hit.point;

         return true;
      }
      
      transform.LookAt(_centerMonolit);
      _lastPickDelay -= Time.deltaTime;
      
      _renderContainer.localPosition = Vector3.zero;
      return false;
   }

   private void ClearCoroutine()
   {
      if (_cor != null)
      {
         StopCoroutine(_cor);
         _cor = null;
      }
   }

   public void DoPick(Ray ray, RaycastHit hit)
   {
      var collisions = Physics.OverlapCapsule(_pointStartRaycast, hit.point, _radiusSphere);
      if (collisions.Length > 0)
      {
         for (int i = 0; i < collisions.Length; i++)
         {
            if (collisions[i].attachedRigidbody
                && collisions[i].TryGetComponent(out RayfireRigid rayFireRigid))
            {
               
               EventManager.EmitEvent(GameEventType.SUCCESS_PICK_HIT, hit);

               if (rayFireRigid.demolitionType == DemolitionType.ReferenceDemolition)
               {
                  rayFireRigid.Demolish();
                  Destroy(rayFireRigid.gameObject);
               } else
               {
                  if (rayFireRigid.limitations.currentDepth > 0)
                  {
                     rayFireRigid.Activate();
                     var rb = rayFireRigid.GetComponent<Rigidbody>();
                     rb.AddForce(ray.direction * -100f);
                  } else
                  {
                     rayFireRigid.Demolish();
                  }
               }
               
               return;
               
               if (rayFireRigid.limitations.currentDepth > 0 || true)
               {
                  var collider = rayFireRigid.GetComponent<SphereCollider>();
                  collider.radius *= .7f;
                  if (rayFireRigid.GetComponent<Rigidbody>())
                  {
                     rayFireRigid.Activate();
                  }
                  
                  var directionForce = Vector3.Cross(ray.direction, Randombool ? Vector3.up : Vector3.down) * .3f
                                       + Vector3.up * .2f;
                  collisions[i].attachedRigidbody.AddForce(directionForce * _force);
                  collisions[i].gameObject.layer = GameConfig.LayerStonePicked;
               } else
               {
                  rayFireRigid.Demolish();
               }
            }
         }
      }
   }
}