using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _objectsForEnable;

    private float _timerNext = 0;
    
    private IEnumerator Start()
    {
        for (int i = 0; i < _objectsForEnable.Length; i++)
        {
            _objectsForEnable[i].SetActive(true);
            yield return new WaitForFixedUpdate();
        }
        
        var _async = SceneManager.LoadSceneAsync("Game");
        yield return new WaitUntil(() => _async.isDone);
        
        _async.allowSceneActivation = true;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _timerNext += Time.deltaTime;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _timerNext = 0;
        }
    }
}
