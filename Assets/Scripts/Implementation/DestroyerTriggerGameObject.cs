using System;
using System.Collections;
using System.Collections.Generic;
using RayFire;
using UnityEngine;

public class DestroyerTriggerGameObject : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.GetComponent<TagNonDestroyerTrigger>())
        {
            StartCoroutine(ProcessDestroy(other.transform));
        }
    }

    private IEnumerator ProcessDestroy(Transform tr)
    {
        var wait = new WaitForEndOfFrame();
        float t = 2f;
        float defaultT = 2f;
        var target = new Vector3(.01f, .01f, .01f);
        
        while (t > 0)
        {
            if (!tr)
            {
                yield break;
            }
            tr.localScale = Vector3.Lerp(target, tr.localScale, t / defaultT);
            t -= Time.deltaTime; 
            yield return wait;
        }

        if (tr)
        {
            tr.gameObject.SetActive(false);
        }
    }
}
