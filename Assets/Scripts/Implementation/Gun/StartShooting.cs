using System;
using System.Collections;
using System.Collections.Generic;
using RayFire;
using UnityEngine;

public class StartShooting : MonoBehaviour
{
    private RayfireGun _rayfireGun;

    private void Awake()
    {
        _rayfireGun = GetComponent<RayfireGun>();
    }

    private void Start()
    {
        _rayfireGun.StartShooting();
    }
}
