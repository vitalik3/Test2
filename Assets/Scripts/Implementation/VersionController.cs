using MyUtilites.Saves;
using System.Linq;
using UnityEngine;

public class VersionController : MonoBehaviour
{
   private void Awake()
   {
      if (string.IsNullOrEmpty(Saves.ApplicationVersion.Value))
      {
         Saves.ApplicationVersion.Value = Application.version;
         EventsAnalytics.Instance.SessionFirst();
      } else if (Saves.ApplicationVersion.Value != Application.version)
      {
         Saves.ApplicationVersion.Value = Application.version;
      }
   }
}