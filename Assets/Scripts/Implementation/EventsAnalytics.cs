﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Console = MyUtilites.Console;

[DefaultExecutionOrder(-102)]
public class EventsAnalytics : MonoBehaviour
{
   private const string SESSION_FIRST = "session_first";
   private const string SESSION_START = "session_start";
   private const string LEVEL_STARTED = "level_started";
   private const string LEVEL_COMPLETED = "level_completed";
   private const string LEVEL_FAILED = "level_failed";
   private const string LEVEL_RESTART = "level_restart";

   public static EventsAnalytics Instance = null;


   private void Awake()
   {
      if (!Instance)
      {
         Instance = this;
         DontDestroyOnLoad(gameObject);
      } else
      {
         DestroyImmediate(gameObject);
      }
   }

   public void SessionFirst()
   {
      LogEvent(SESSION_FIRST);
      AppMetrica.Instance.SendEventsBuffer();
   }
   
   public void SessionStart()
   {
      LogEvent(SESSION_START);
      AppMetrica.Instance.SendEventsBuffer();
   }
   
   public void LevelStarted()
   {
      LogEvent(LEVEL_STARTED);
      AppMetrica.Instance.SendEventsBuffer();
   }
   
   public void LevelCompleted()
   {
      LogEvent(LEVEL_COMPLETED);
      AppMetrica.Instance.SendEventsBuffer();
   }
   
   public void LevelFailed()
   {
      LogEvent(LEVEL_FAILED);
      AppMetrica.Instance.SendEventsBuffer();
   }
   
   public void LevelRestart()
   {
      LogEvent(LEVEL_RESTART);
      AppMetrica.Instance.SendEventsBuffer();
   }
   
   //public void AnalyticsEventUpgrade(string skillName, string typePayUpgrade, int progressLevelUpgrade)
   //{
   //   var data = new Dictionary<string, object>
   //   {
   //      {SKILL_NAME, skillName},
   //      {SKILL_UP_LEVEL_NUMBER, progressLevelUpgrade},
   //      {SKILL_PAY_TYPE, typePayUpgrade},
   //   };
   //   LogEvent(UPGRADE, data);
   //   AppMetrica.Instance.SendEventsBuffer();
   //}

  
   public void LogEvent(string id, Dictionary<string, object> data = null)
   {
      if (data != null)
      {
         AppMetrica.Instance.ReportEvent(id, data);
         foreach (var o in data)
         {
            Console.Print("LogEvent: " + id + " key: " + o.Key + " value: " + o.Value);
         }
      } else
      {
         AppMetrica.Instance.ReportEvent(id);
         Console.Print("LogEvent: " + id);
      }
   }
}