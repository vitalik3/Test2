using MyUtilites;
using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class CameraManager : MonoBehaviour
{
   [SerializeField] private Camera _camera;
   [SerializeField] private CinemachineVirtualCamera _virtualPlayerFollower;
   [SerializeField] private CinemachineBrain _brain;

   private Vector3 _cashSmoothDamp;
   private CameraTarget _targetFollow;
   private CameraTarget _targetLookAt;
   

   private void OnInitCameraTargetPlayer(object target)
   {
      var cameraTrget = (CameraTarget) target;
      
      if (cameraTrget.Type == CameraTarget.TypeCameraTarget.FOLLOW)
      {
         _targetFollow = cameraTrget;
         _virtualPlayerFollower.Follow = _targetFollow.transform;
      } else
      {
         _targetLookAt = cameraTrget;
         _virtualPlayerFollower.LookAt = _targetLookAt.transform;
      }
   }

   private void OnInitCameraTargetBuild(object target)
   {
      
   }

   private void UpdateTarget(object _)
   {
      
   }

   private void OnDeadPlayer(object _)
   {
      _virtualPlayerFollower.Follow = null;
      _virtualPlayerFollower.LookAt = null;
   }

   private void OnEnable()
   {
      EventManager.StartListening(GameEventType.INIT_CAMERA_TARGET, OnInitCameraTargetPlayer);
      EventManager.StartListening(GameEventType.COLLECT_TARGET_SPHERE, OnInitCameraTargetBuild);
      EventManager.StartListening(GameEventType.UPDATE_TARGET_CAMERA, UpdateTarget);
      EventManager.StartListening(GameEventType.PLAYER_DEAD, OnDeadPlayer);
   }

   private void OnDisable()
   {
      EventManager.StopListening(GameEventType.INIT_CAMERA_TARGET, OnInitCameraTargetPlayer);
      EventManager.StopListening(GameEventType.COLLECT_TARGET_SPHERE, OnInitCameraTargetBuild);
      EventManager.StopListening(GameEventType.UPDATE_TARGET_CAMERA, UpdateTarget);
      EventManager.StopListening(GameEventType.PLAYER_DEAD, OnDeadPlayer);
   }
}