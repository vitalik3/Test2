using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseButton : MonoBehaviour
{
    [SerializeField] protected Button _button;

    protected void Start()
    {
        _button.onClick.AddListener(OnClick);
    }

    protected virtual void OnClick()
    {
        
    }

    protected void OnDestroy()
    {
        _button.onClick.RemoveListener(OnClick);
    }
}
