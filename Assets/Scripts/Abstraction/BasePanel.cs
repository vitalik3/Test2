using  Modules.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePanel : MonoBehaviour, IPanel
{ 
    [SerializeField] protected int _id;

    public int Id => _id;

    public virtual IEnumerator Show()
    {
        gameObject.SetActive(true);
        yield return null;
    }

    public virtual IEnumerator Hide()
    {
        gameObject.SetActive(false);
        yield return null;
    }
}
