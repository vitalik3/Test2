using System.Collections;
using Modules.UI;
using MyUtilites;
using UnityEngine;
using UnityEngine.Rendering;
using Saves = MyUtilites.Saves.Saves;

[DefaultExecutionOrder(-100)]
public class GameManager : MonoBehaviour
{
   public static GameManager Instance = null;

   private Level _currentLevel;

   public Level LevelActive => _currentLevel;
   

   private void Awake()
   {
      if (Instance)
      {
         DestroyImmediate(gameObject);
      } else
      {
         Instance = this;
         DontDestroyOnLoad(gameObject);
         if (EventsAnalytics.Instance)
         {
            EventsAnalytics.Instance.SessionStart();
         }
      }
   }

   private void Start()
   {
      LoadLevel();
   }

   private void LoadLevel()
   {
#if UNITY_EDITOR
      _currentLevel = FindObjectOfType<Level>();
#endif
      if (!_currentLevel)
      {
         DestroyLevel();
         if (GameConfig.Instance.GenerateLevelOnStart)
         {
            CreateLevel();
         }
      }
   }

   private void Restart()
   {
      DestroyLevel();
      CreateLevel();
      UI.Instance.Open(0);
   }

   private void CreateLevel()
   {
      var selectLevel = GameConfig.Instance.PrefLevels[Saves.CurrentLevelRunPath.Value % GameConfig.Instance.PrefLevels.Count];
      _currentLevel = Instantiate(selectLevel, Vector3.zero, Quaternion.identity);
   }

   private void DestroyLevel()
   {
      if (_currentLevel)
      {
         Destroy(_currentLevel.gameObject);
      }
   }

   private void OnClickTapToRestart(object _)
   {
      EventsAnalytics.Instance.LevelRestart();
      Restart();
   }

   private void OnClickTapToStart(object _)
   {
      InputController.Enable = true;
      EventsAnalytics.Instance.LevelStarted();
      UI.Instance.Open(1);
   }

   private void OnCollectTargetSphere(object _)
   {
      Saves.CurrentLevelRunPath.Value++;
      EventsAnalytics.Instance.LevelCompleted();
      
      StartCoroutine(Delay());

      IEnumerator Delay()
      {
         yield return new WaitForSeconds(GameConfig.Instance.DelayRestart);
         if (GameConfig.Instance.EnableUIExtendedPanel)
         {
            UI.Instance.Open(2);
         } else
         {
            Restart();
         }
      }
   }

   private void PlayerDead(object _)
   {
      EventsAnalytics.Instance.LevelFailed();
      StartCoroutine(Delay());
      IEnumerator Delay()
      {
         yield return new WaitForSeconds(GameConfig.Instance.DelayRestart);
         if (GameConfig.Instance.EnableUIExtendedPanel)
         {
            UI.Instance.Open(3);
         } else
         {
            Restart();
         }
      }
   }

   private void OnClickNextLevel(object _)
   {
      EventManager.EmitEvent(GameEventType.CLICK_TAP_TO_RESTART);
   }

   private void OnEnable()
   {
      EventManager.StartListening(GameEventType.CLICK_TAP_TO_START, OnClickTapToStart);
      EventManager.StartListening(GameEventType.CLICK_TAP_TO_NEXT_LEVEL, OnClickNextLevel);
      EventManager.StartListening(GameEventType.COLLECT_TARGET_SPHERE, OnCollectTargetSphere);
      EventManager.StartListening(GameEventType.PLAYER_DEAD, PlayerDead);
      EventManager.StartListening(GameEventType.CLICK_TAP_TO_RESTART, OnClickTapToRestart);
      EventManager.StartListening(GameEventType.CLICK_TAP_TO_TRY_AGAIN, OnClickTapToRestart);
   }

   private void OnDisable()
   {
      EventManager.StopListening(GameEventType.CLICK_TAP_TO_START, OnClickTapToStart);
      EventManager.StopListening(GameEventType.CLICK_TAP_TO_NEXT_LEVEL, OnClickNextLevel);
      EventManager.StopListening(GameEventType.COLLECT_TARGET_SPHERE, OnCollectTargetSphere);
      EventManager.StopListening(GameEventType.PLAYER_DEAD, PlayerDead);
      EventManager.StopListening(GameEventType.CLICK_TAP_TO_RESTART, OnClickTapToRestart);
      EventManager.StopListening(GameEventType.CLICK_TAP_TO_TRY_AGAIN, OnClickTapToRestart);
   }
}