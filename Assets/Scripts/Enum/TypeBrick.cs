using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeBrick
{
    BROWN,
    RED,
    PURPLE,
    YELLOW,
    GREEN
}
