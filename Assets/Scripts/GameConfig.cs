using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

[DefaultExecutionOrder(-101)]
public class GameConfig : MonoBehaviour
{
    public static GameConfig Instance = null;
    public static int LayerSphere;
    public static int LayerSphereNeutral;
    public static int LayerStonePicked;

    [Header("Фэйковые сохранения")]
    [SerializeField] private List<int> _fakeProgressSave;
    [Header("Задержка победы и перезагрузка уровня")]
    [SerializeField] private float delayRestart = 2f;
    [Header("Включает в цикл UI win/lose")]
    [SerializeField] private bool _enableUIExtendedPanel = false;
    [Header("Создавать при старте уровени или нет")]
    [SerializeField] private bool _generateLevelOnStart = true;
    [Header("Включенные в геймплей уровни")]
    [SerializeField] private List<Level> _prefLevels;
    [Header("Включенные в геймплей финиши")]
    [SerializeField] private List<Finish> _prefFinishes;
    [Header("Игрок всегда бежит или только по нажатию клавиши")]
    [SerializeField] private bool _alwaysRun;
    [SerializeField] private float _mouseSenceX;
    [Space()]
    [Header( "For develop==============================================================" )]
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _prefRoad;
    [SerializeField] private List<Obstacle> _prefObstacles;
    [SerializeField] private Player[] _players;
    [SerializeField] private Material[] _materialsSphere;
   

    public List<int> FakeProgressSave => _fakeProgressSave;
    public float DelayRestart => delayRestart;
    public bool EnableUIExtendedPanel => _enableUIExtendedPanel;
    public bool GenerateLevelOnStart => _generateLevelOnStart;
    public bool AlwaysRun => _alwaysRun;
    public float MouseSence => _mouseSenceX;
    public List<Level> PrefLevels => _prefLevels;
    public List<Finish> PrefFinishes => _prefFinishes;
    public GameObject Player => _player;
    public GameObject PrefRoad => _prefRoad;
    public List<Obstacle> PrefObstacles => _prefObstacles;
    public Player[] Players => _players;
    public Material[] MaterialsSphere => _materialsSphere;


    private void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            LayerSphere = LayerMask.NameToLayer(GameTags.SPHERE);
            LayerSphereNeutral = LayerMask.NameToLayer(GameTags.SPHERE_NEUTRAL);
            LayerStonePicked = LayerMask.NameToLayer(GameTags.STONE_PICKED);
            DontDestroyOnLoad(gameObject);
        }
    }

    public Material GetRandomMaterialSphere()
    {
        return _materialsSphere[Random.Range(0, _materialsSphere.Length)];
    }
}
