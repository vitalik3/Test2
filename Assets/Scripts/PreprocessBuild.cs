/*using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
using UnityEngine;

public class PreprocessBuild : MonoBehaviour, IPreprocessBuildWithReport
{
   private const string DEBUG_PATH = "/zPlugins/DebugPanel";
   private const string RELEASE_PATH = "/zPlugins/DebugPanel~";

   public int callbackOrder { get; }

   public void OnPreprocessBuild(BuildReport report)
   {
      var buildPlayerOptions = GetBuildPlayerOptions();

      if ((buildPlayerOptions.options & BuildOptions.Development) != 0)
      {
         Debug.LogWarning("DEVELOPMENT BUILD");
      } else
      {
         Debug.LogWarning("RELEASE BUILD");

         var debugPath = Application.dataPath + DEBUG_PATH;
         var releasePath = Application.dataPath + RELEASE_PATH;
         if (IsDirectoryExists(debugPath))
         {
            Debug.LogWarning("Move " + debugPath + " to " + releasePath);
            Directory.Move(debugPath, releasePath);
            AssetDatabase.Refresh();
         } else
         {
            Debug.LogWarning("Folder not find");
         }
      }
   }

   [PostProcessBuild]
   private static void OnPostprocessBuild(BuildTarget target, string pathToBuildProject)
   {
      Debug.LogWarning("OnPostprocessBuild");

      var debugPath = Application.dataPath + DEBUG_PATH;
      var releasePath = Application.dataPath + RELEASE_PATH;

      if (IsDirectoryExists(releasePath))
      {
         Debug.LogWarning("Move " + releasePath + " to " + debugPath);
         if (IsDirectoryExists(debugPath))
         {
            Directory.Delete(debugPath);
         }

         Directory.Move(releasePath, debugPath);
         AssetDatabase.Refresh();
      } else
      {
         Debug.LogWarning("Folder not find");
      }
   }

   private static bool IsDirectoryExists(string dir)
   {
      return Directory.Exists(Path.Combine(dir));
   }

   private static BuildPlayerOptions GetBuildPlayerOptions(bool askForLocation = false, BuildPlayerOptions defaultOptions = new BuildPlayerOptions())
   {
      MethodInfo method =
         typeof(BuildPlayerWindow.DefaultBuildMethods).GetMethod("GetBuildPlayerOptionsInternal", BindingFlags.NonPublic | BindingFlags.Static);
      return (BuildPlayerOptions) method.Invoke(null, new object[] {askForLocation, defaultOptions});
   }
}*/