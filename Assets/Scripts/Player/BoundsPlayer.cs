using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsPlayer : MonoBehaviour
{
    [SerializeField] private float _boundsX;

    public float BoundsX => _boundsX;
}
