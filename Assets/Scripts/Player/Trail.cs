using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
   private static Dictionary<Color, Material> _materials = new Dictionary<Color, Material>();
   
   [SerializeField] private float _offsetRand;
   [SerializeField] private TrailRenderer _trail;

   private Sphere _host;
   private float _currentOffset;
   
   public void Init(Sphere host, Color color)
   {
      _trail.enabled = true;
      _host = host;
      _trail.material.color = color;
      _currentOffset = Random.Range(-_offsetRand, _offsetRand);
      enabled = true;
      
      //UpdatePositionFromHost();
   }
   
   private void UpdatePositionFromHost()
   {
      _trail.transform.position = _host.transform.position - new Vector3(0, _host.Radius - _currentOffset, 0);
   }
   
   private void FixedUpdate()
   {
      //UpdatePositionFromHost();
   }
}
