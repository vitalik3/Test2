using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;
using UnityEngine.TextCore;

public class PlayerAnimatorControlller : MonoBehaviour
{
    private const string RUN = "running";
    private const string FALL = "fall";

    [SerializeField] private Animator _anim;

    private Vector3 _lastPosition;
    private float _lastSpeed;

    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        _anim.enabled = true;
    }

    public void UpdateSpeed(float speed)
    {
        _anim.SetFloat(RUN, speed);
    }

    private void PlayerDead(object _)
    {
        _anim.SetTrigger(FALL);
    }

    private void OnEnable()
    {
        EventManager.StartListening(GameEventType.PLAYER_DEAD, PlayerDead);
    }

    private void OnDisable()
    {
        EventManager.StopListening(GameEventType.PLAYER_DEAD, PlayerDead);
    }
}
