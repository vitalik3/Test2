using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;

public class SphereNeutral : MonoBehaviour
{
   private void Start()
   {
      gameObject.layer = GameConfig.LayerSphereNeutral;
   }

   private void OnCollisionEnter(Collision other)
   {
      if (other.gameObject.layer == GameConfig.LayerSphere && TryGetComponent(out Sphere selfSphere))
      {
         selfSphere.SetColor(GameConfig.Instance.GetRandomMaterialSphere());
         Destroy(this);
      }
   }
}
