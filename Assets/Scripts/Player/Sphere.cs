using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using MyUtilites;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Sphere : MonoBehaviour
{
    [SerializeField] private float _radius;
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private Renderer _renderer;
    [SerializeField] private Trail _trail;

    private Color _mainColor;

    public Color MainColor => _mainColor;
    public float Radius => _radius;
    public Rigidbody Rb => _rb;


    private void Start()
    {
        EventManager.EmitEvent(GameEventType.SPHERE_DID_CREATED, this);
    }

    public void Init(Material materialSphere)
    {
        
        InitTrail();
    }
   
    public void SetColor(Material materialSphere)
    {
        if (TryGetComponent(out SphereNeutral neutral))
        {
            Destroy(neutral);
        }
        
        _renderer.material = materialSphere;
        _mainColor = materialSphere.color;
        gameObject.layer = GameConfig.LayerSphere;
        InitTrail();
        EventManager.EmitEvent(GameEventType.SPHERE_COLORING, this);
    }

    public void Kill()
    {
        EventManager.EmitEvent(GameEventType.SPHERE_WILL_DESTROY, this);
        Destroy(_trail);
        Destroy(gameObject);
    }
    
    private void InitTrail()
    {
        _trail.Init(this, MainColor);
    }
}
