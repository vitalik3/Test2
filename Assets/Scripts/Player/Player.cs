using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using MyUtilites;
using UnityEngine;
using UnityEngine.TextCore;

public class Player : MonoBehaviour
{
   [SerializeField] private InputController _inp;

   private bool _didKill;

   public void Kill()
   {
      if (_didKill)
      {
         return;
      }
      
      _didKill = true;
      DisableInputController();
      Destroy(GetComponent<Rigidbody>());
      Destroy(GetComponent<Collider>());
      
      EventManager.EmitEvent(GameEventType.PLAYER_DEAD);
   }

   public void DisableInputController()
   {
      _inp.enabled = false;
   }
}
