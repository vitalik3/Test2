using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;

public class InputController : MonoBehaviour
{
   public static bool Enable = true;
   
   [SerializeField] private Rigidbody _rb;
   [SerializeField] private BoundsPlayer _bounds;
   [SerializeField] private PlayerAnimatorControlller _animController;

   private Coroutine _cor;
   private Vector3 _currentInput;
   
   private Vector2 GetInput => new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

   private void Update()
   {
      if (Input.GetMouseButtonDown(0) && _cor == null && Enable)
      {
         _cor = StartCoroutine(InputHandler());
      }
   }

   private IEnumerator InputHandler()
   {
      var wait = new WaitForEndOfFrame();
      _currentInput = GetInput;

      while (Input.GetMouseButton(0))
      {
         _currentInput = GetInput;
         yield return wait;
      }

      _currentInput = Vector3.zero;
      _animController.UpdateSpeed(0f);
      _cor = null;
   }

   private void FixedUpdate()
   {
      var speedForward = !GameConfig.Instance.AlwaysRun && _cor == null ? 0 : 1f;
      var move = speedForward * transform.forward
                 + _currentInput.x * GameConfig.Instance.MouseSence * transform.right;

      _animController.UpdateSpeed(speedForward);
      
      float bounds = _bounds.BoundsX;
      float expectedPositionX = (transform.position + move).x;
         
      if ( expectedPositionX > bounds && move.x > 0 || expectedPositionX < -bounds && move.x < 0)
      {
         if (expectedPositionX > 0)
         {
            move.x = bounds - transform.position.x;
         } else
         {
            move.x = -bounds - transform.position.x;
         }
      }

      _rb.MovePosition(transform.position + move);
   }

   private void ClearCoroutine()
   {
      if (_cor != null)
      {
         StopCoroutine(_cor);
         _currentInput = Vector3.zero;
         _cor = null;
      }
   }

   private void OnDisable()
   {
      ClearCoroutine();
   }
}