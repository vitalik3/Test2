﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

   [CreateAssetMenu(fileName = "DataGate", menuName = "ScriptableObjects/DataGate", order = 1)]
   public class DataGate : ScriptableObject
   {
      private const string STRING_MULTIPLY = "x";
      private const string STRING_ADD = "+";
      private const string STRING_MINUS = "-";
      
      public TypeMathSymbol simbol;
      public int value;

      public static bool IsBad(DataGate dg)
      {
         if (dg.simbol == TypeMathSymbol.ADD || dg.simbol == TypeMathSymbol.MULTIPLY)
         {
            return false;
         }

         return true;
      }
      
      public static string GetString(DataGate dg)
      {
         if (dg.simbol == TypeMathSymbol.ADD)
         {
            return STRING_ADD + dg.value;
         }

         if (dg.simbol == TypeMathSymbol.MULTIPLY)
         {
            return STRING_MULTIPLY + dg.value;
         }
         
         if (dg.simbol == TypeMathSymbol.MINUS)
         {
            return STRING_MINUS + dg.value;
         }

         return dg.value.ToString();
      }

      public static int GetComputeResult(int inputCount, TypeMathSymbol type, int value)
      {
         int result = 0;

         if (type == TypeMathSymbol.MULTIPLY)
         {
            return inputCount * value;
         }
         
         if (type == TypeMathSymbol.ADD)
         {
            return inputCount + value;
         }
         
         if (type == TypeMathSymbol.MINUS)
         {
            return inputCount - value;
         }

         return result;
      }
      
      public static int GetComputeResult(int inputCount, DataGate dg)
      {
         int result = 0;

         if (dg.simbol == TypeMathSymbol.MULTIPLY)
         {
            return inputCount * dg.value;
         }
         
         if (dg.simbol == TypeMathSymbol.ADD)
         {
            return inputCount + dg.value;
         }
         
         if (dg.simbol == TypeMathSymbol.MINUS)
         {
            return inputCount - dg.value;
         }

         return result;
      }
      
      public static int GetCountAddValue (int inputCount, TypeMathSymbol type, int value)
      {
         int result = 0;

         if (type == TypeMathSymbol.MULTIPLY)
         {
            result = inputCount * value - inputCount;
            return result;
         }
         
         if (type == TypeMathSymbol.ADD)
         {
            return value;
         }
         
         if (type == TypeMathSymbol.MINUS)
         {
            return -value;
         }

         return result;
      }
      
      public static int GetCountAddValue (int inputCount, DataGate dg)
      {
         int result = 0;

         if (dg.simbol == TypeMathSymbol.MULTIPLY)
         {
            result = inputCount * dg.value - inputCount;
            return result;
         }
         
         if (dg.simbol == TypeMathSymbol.ADD)
         {
            return dg.value;
         }
         
         if (dg.simbol == TypeMathSymbol.MINUS)
         {
            return -dg.value;
         }

         return result;
      }
   }

