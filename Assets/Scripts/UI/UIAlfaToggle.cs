using UnityEngine;

public class UIAlfaToggle : MonoBehaviour
{
   [SerializeField] private CanvasGroup [] _canvasGroup;
   
   
   public void Toggle()
   {
      foreach (var canvasGrop in _canvasGroup)
      {
         if (canvasGrop)
         {
            canvasGrop.alpha = canvasGrop.alpha > 0 ? 0 : 1f;
         }
      }
   }
   
   private void Update()
   {
      if (Input.GetKeyDown(KeyCode.H))
      {
         Toggle();
      }
   }

 
}