using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;

public class LoseMenu : BasePanel
{
   public void OnClickTryAgaine()
   {
      EventManager.EmitEvent(GameEventType.CLICK_TAP_TO_TRY_AGAIN);
   }
}
