using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;

public class WinMenu : BasePanel
{
    public void OnClickNextLevel()
    {
        EventManager.EmitEvent(GameEventType.CLICK_TAP_TO_NEXT_LEVEL);
    }
}
