using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;
using UnityEngine.UI;

public class RestartButton : BaseButton
{
    protected override void OnClick()
    {
        EventManager.EmitEvent(GameEventType.CLICK_TAP_TO_RESTART);
    }
}
