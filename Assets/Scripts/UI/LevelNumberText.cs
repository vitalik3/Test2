using MyUtilites.Saves;
using TMPro;
using UnityEngine;

public class LevelNumberText : MonoBehaviour
{
    private const string LEVEL = "LEVEL: ";
    
    [SerializeField] private TextMeshProUGUI _text;

    private void Start()
    {
        Saves.CurrentLevel.OnChanged += ReDraw;
        ReDraw(0);
    }

    private void ReDraw(int value)
    {
        _text.text = LEVEL + (Saves.CurrentLevel.Value + 1);
    }

    private void OnDestroy()
    {
        Saves.CurrentLevel.OnChanged -= ReDraw;
    }
}
