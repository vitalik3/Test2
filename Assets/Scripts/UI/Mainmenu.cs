using System;
using Modules.UI;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;
using UnityEngine.TextCore;

public class Mainmenu : BasePanel
{
    private void OnEnable()
    {
        InputController.Enable = false;
    }

    public void OnClickTapToStart()
    {
        EventManager.EmitEvent(GameEventType.CLICK_TAP_TO_START);
    }
}
