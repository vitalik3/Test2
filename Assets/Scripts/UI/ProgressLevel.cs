using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using TMPro;
using UnityEngine;

public class ProgressLevel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;

    private Level _level;

    private void ReDraw(int currentColor, int full)
    {
        _text.text = currentColor + " / " + full;
    }
    
    private void OnInitNewLevel(object level)
    {
        _level = (Level) level;
        //_level.OnChangeSphereStatus += ReDraw;
    }
    
    private void OnEnable()
    {
        EventManager.StartListening(GameEventType.LEVEL_DID_LOAD, OnInitNewLevel);
    }

    private void OnDisable()
    {
        EventManager.StopListening(GameEventType.LEVEL_DID_LOAD, OnInitNewLevel);
    }
}
