using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteSaves : BaseButton
{
    protected override void OnClick()
    {
        PlayerPrefs.DeleteAll();
    }
}
