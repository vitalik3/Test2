using System.Collections;
using System.Collections.Generic;
using MyUtilites.Saves;
using UnityEngine;

public class NextPalitraButton : BaseButton
{
    protected override void OnClick()
    {
        Saves.SelectPalitraTexture.Value++;
    }
}
