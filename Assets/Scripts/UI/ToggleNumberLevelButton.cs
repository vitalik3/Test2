using MyUtilites;
using MyUtilites.Saves;
using UnityEngine;

public class ToggleNumberLevelButton : BaseButton
{
   [SerializeField] private int _countAddNumLevel;
   protected override void OnClick()
   {
      Saves.CurrentLevel.Value += _countAddNumLevel;
      EventManager.EmitEvent(GameEventType.CLICK_TAP_TO_RESTART);
   }
}
