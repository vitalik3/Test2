using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftParent : MonoBehaviour
{
   private Transform _target;
   private Level _level;

   private void Start()
   {
      _target = transform.parent;
      _level = GetComponentInParent<Level>();
      transform.SetParent(_level.transform);
   }

   private void FixedUpdate()
   {
      if (_target)
      {
         transform.position = _target.position;
         transform.rotation = _target.rotation;
      }
   }
}
