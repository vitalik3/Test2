using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameEventType
{
    public const string WIN = "WIN";
    public const string LOSE = "LOSE";
    public const string CLICK_TAP_TO_START = "CLICK_TAP_TO_START";
    public const string CLICK_TAP_TO_NEXT_LEVEL = "CLICK_TAP_TO_NEXT_LEVEL";
    public const string CLICK_TAP_TO_TRY_AGAIN = "CLICK_TAP_TO_TRY_AGAIN";
    public const string CLICK_TAP_TO_RESTART = "CLICK_TAP_TO_RESTART";
    public const string LEVEL_DID_LOAD = "LEVEL_DID_LOAD";
    
    public const string UPDATE_TARGET_CAMERA = "UPDATE_TARGET_CAMERA";
    public const string CREAT_CAMERA_PIVOT = "CREAT_CAMERA_PIVOT";
    public const string INIT_PLAYER = "INIT_PLAYER";
    public const string INIT_CAMERA_TARGET = "INIT_CAMERA_TARGET";
    public const string PLAYER_DEAD = "PLAYER_DEAD";
    public const string COLLECT_TARGET_SPHERE = "COLLECT_TARGET_SPHERE";
    public const string FINISH_CAMERA_RETARGET = "FINISH_CAMERA_RETARGET";
    public const string SPHERE_DID_CREATED = "SPHERE_DID_CREATED";
    public const string SPHERE_WILL_DESTROY = "SPHERE_WILL_DESTROY";
    public const string SPHERE_COLORING = "SPHERE_COLORING";
    public const string SUCCESS_PICK_HIT = "SUCCESS_PICK_HIT";
    
}
