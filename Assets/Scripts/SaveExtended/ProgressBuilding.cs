using System;
using System.Collections.Generic;
using UnityEngine;

using Saves = MyUtilites.Saves.Saves;

public static class ProgressBuilding
{
   private const string KEY_BUILDING = "KEY_BUILDING";

   [SerializeField]
   public class CommonDataProgressBuilding
   {
      public List<DataLevelProgressBuilding> levelsDataBuild;
   }

   [Serializable]
   public class DataLevelProgressBuilding
   {
      public List<int> collectBricks;
   }

   private static CommonDataProgressBuilding _data;

   public static List<int> GetData()
   {
      if (GameConfig.Instance && GameConfig.Instance.FakeProgressSave.Count > 0)
      {
         var result = GameConfig.Instance.FakeProgressSave;
         int buildCountFirst = result[0];
         int countAdd = 5 - result.Count;
         for (int i = 0; i < countAdd; i++)
         {
               result.Add(buildCountFirst);
         }
         return result;
      }
      
      if (_data == null)
      {
         Load();
      }

      if (_data.levelsDataBuild == null || _data.levelsDataBuild.Count <= Saves.CurrentLevel.Value)
      {
         return null;
      }
      
      return _data.levelsDataBuild[Saves.CurrentLevel.Value].collectBricks;
   }

   public static void Save(List<int> collectBricks)
   {
      if (GameConfig.Instance && GameConfig.Instance.FakeProgressSave.Count > 0)
      {
         return;
      }
      
      if (_data.levelsDataBuild.Count > Saves.CurrentLevel.Value)
      {
         _data.levelsDataBuild[Saves.CurrentLevel.Value].collectBricks = collectBricks;
      } else
      {
         var progressLevel = new DataLevelProgressBuilding();
         progressLevel.collectBricks = collectBricks;
         _data.levelsDataBuild.Add(progressLevel);
      }

      var str = JsonUtility.ToJson(_data);
      PlayerPrefs.SetString(KEY_BUILDING, str);
      PlayerPrefs.Save();
   }

   private static void Load() 
   {
      if (PlayerPrefs.HasKey(KEY_BUILDING))
      {
         _data = JsonUtility.FromJson<CommonDataProgressBuilding>(PlayerPrefs.GetString(KEY_BUILDING));
      } else
      {
         _data = new CommonDataProgressBuilding();
         _data.levelsDataBuild = new List<DataLevelProgressBuilding>();
         PlayerPrefs.SetString(KEY_BUILDING, JsonUtility.ToJson(_data));
      }
   }
}