using System;
using System.Collections;
using System.Collections.Generic;
using RayFire;
using UnityEngine;

public class PickTest : MonoBehaviour
{
    [SerializeField] private float _radiusSphere;
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                var collisions = Physics.OverlapSphere(hit.point, _radiusSphere);
                if (collisions.Length > 0)
                {
                    for (int i = 0; i < collisions.Length; i++)
                    {
                        if (collisions[i].attachedRigidbody && collisions[i].TryGetComponent(out RayfireRigid rf))
                        {
                            if (rf.demolitionType == DemolitionType.ReferenceDemolition)
                            {
                                rf.Demolish();
                                //Destroy(rf.gameObject);
                            } else
                            {
                                if (rf.limitations.currentDepth > 0)
                                {
                                    rf.Activate();
                                    var rb = rf.GetComponent<Rigidbody>();
                                    rb.AddForce(ray.direction * -100f);
                                } else
                                {
                                    rf.Demolish();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
