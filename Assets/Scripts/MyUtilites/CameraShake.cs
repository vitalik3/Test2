﻿using MyUtilites;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyUtilites
{
     public class CameraShake : MonoBehaviour
     {
          public float duration;
          public float shakeAmount;//The amount to shake this frame.
          public float shakeDuration;//The duration this frame.
          public bool smooth;//Smooth rotation?
          public float smoothAmount = 5f;//Amount to smooth

          private float shakePercentage;//A percentage (0-1) representing the amount of shake to be applied when setting rotation.
          private float startAmount;//The initial shake amount (to determine percentage), set when ShakeCamera is called.
          private float startDuration;//The initial shake duration, set when ShakeCamera is called.

          private bool isRunning = false;  //Is the coroutine running right now?


          public void ShakeCamera( float amount, float duration )
          {
               shakeAmount += amount;//Add to the current amount.
               startAmount = shakeAmount;//Reset the start amount, to determine percentage.
               shakeDuration += duration;//Add to the current time.
               startDuration = shakeDuration;//Reset the start time.

               if ( !isRunning )
               {
                    StartCoroutine( Shake( ) );//Only call the coroutine if it isn't currently running. Otherwise, just set the variables.

               }
          }

          private void Start( )
          {
               startAmount = shakeAmount;
               startDuration = shakeDuration;
          }

          private void ShakeCamera( object _ )
          {
               shakeAmount = startAmount;//Set default (start) values
               shakeDuration = startDuration;//Set default (start) values

               if ( !isRunning ) StartCoroutine( Shake( ) );//Only call the coroutine if it isn't currently running. Otherwise, just set the variables.
          }

          private void Update( )
          {
               if ( Input.GetKeyDown (KeyCode.S) )
               {
                    ShakeCamera( E.NOTHING );
               }
          }

          private IEnumerator Shake( )
          {
               isRunning = true;
               Quaternion startQ = transform.localRotation;

               while ( shakeDuration > 0.01f )
               {
                    Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;//A Vector3 to add to the Local Rotation
                    rotationAmount.z = 0;//Don't change the Z; it looks funny.

                    shakePercentage = shakeDuration / startDuration;//Used to set the amount of shake (% * startAmount).

                    shakeAmount = startAmount * shakePercentage;//Set the amount of shake (% * startAmount).
                    shakeDuration = Mathf.Lerp( shakeDuration, 0, Time.deltaTime / duration );//Lerp the time, so it is less and tapers off towards the end.

                    if ( smooth )
                         transform.localRotation = Quaternion.Lerp( transform.localRotation, startQ * Quaternion.Euler( rotationAmount ), Time.deltaTime * smoothAmount );
                    else
                         transform.localRotation = startQ * Quaternion.Euler( rotationAmount );//Set the local rotation the be the rotation amount.

                    yield return new WaitForEndOfFrame( );
               }
               transform.localRotation = startQ;
               isRunning = false;
          }

     }
}

