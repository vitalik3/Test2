using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CoroutineHost : MonoBehaviour
{
   public static CoroutineHost Instance = null;

   public static WaitForEndOfFrame WaitForEndOfFrameHost;
   public static WaitForFixedUpdate WaitForFixedUpdateHost;
   
   private void Awake()
   {
      if (Instance)
      {
         DestroyImmediate(gameObject);
      } else
      {
         WaitForEndOfFrameHost = new WaitForEndOfFrame();
         WaitForFixedUpdateHost = new WaitForFixedUpdate();
         Instance = this;
      }
   }

   public static Coroutine StartCoroutineHost(IEnumerator coroutine)
   {
      return Instance.StartCoroutine(coroutine);
   }

   public static void StopCoroutineHost(Coroutine coroutine)
   {
      if (coroutine != null)
      {
         ((MonoBehaviour) Instance).StopCoroutine(coroutine);
         coroutine = null;
      }
   }

   private void OnDisable()
   {
      StopAllCoroutines();
   }
}
