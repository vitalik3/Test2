﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwingPuzzle3D
{
     public class MousPointRender : MonoBehaviour
     {
          [SerializeField] private SpriteRenderer render;


          private void Update( )
          {
               if ( Input.GetMouseButton(0) )
               {
                    render.color = Color.red;
                    transform.position = Camera.main.ScreenPointToRay( Input.mousePosition ).origin;
                    transform.LookAt( Camera.main.transform );
                    //transform.position += Vector3.forward * 9f;
               } else
               {
                    render.color = Color.yellow;
               }
              
          }
     }
}

