using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMainCamera : MonoBehaviour
{
    private Transform _gameCamera;

    private void Update()
    {
        transform.LookAt(_gameCamera);
    }

    private void OnEnable()
    {
        _gameCamera = Camera.main.transform;
    }

}
