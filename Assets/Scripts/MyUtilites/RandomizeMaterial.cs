﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyUtilites
{
     [RequireComponent (typeof(MeshRenderer))]
     public class RandomizeMaterial : MonoBehaviour
     {
          [SerializeField] private List<Material> materials;

          private MeshRenderer _meshRend;


          private void Start( )
          {
               _meshRend = GetComponent<MeshRenderer>( );
               SetRandomMaterisl( );
          }

          private Material SetRandomMaterisl ()
          {
               int random = Random.Range( 0, materials.Count );

               _meshRend.material = materials [ random ];

               return materials [ random ];
          }
     }
}

