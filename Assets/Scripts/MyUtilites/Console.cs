using UnityEngine;

namespace MyUtilites
{
    public static class Console
    {
        public static void Print(string value, LogType logType = LogType.Log)
        {
            if (!Debug.isDebugBuild)
            {
                return;
            }

            switch (logType)
            {
                case LogType.Log:
                    Debug.Log(value);
                    break;
                case LogType.Error:
                    Debug.LogError(value);
                    break;
            }
        }
    }
}
