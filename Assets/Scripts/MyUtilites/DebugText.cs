﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace MyUtilites
{
     public class DebugText : MonoBehaviour
     {
          private static int maxLenghtString = 40;
          private static int currentLenght = 0;
          private static TextMeshProUGUI fieldText;

          public static void RedrawText( string s )
          {
               fieldText.text = fieldText.text + "\n" + s;
               if ( currentLenght > maxLenghtString )
               {
                    fieldText.text = "";
                    currentLenght = 0;
               }
               currentLenght++;
          }

          public static void SetText( string s )
          {
               if ( fieldText != null )
               {
                    fieldText.text = s;
               }
          }

          private void Awake( )
          {
               Init( );
          }

          private void Init( )
          {
               fieldText = GetComponent<TextMeshProUGUI>( );
               fieldText.text = "Init";
          }
     }
}

