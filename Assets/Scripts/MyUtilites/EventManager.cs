﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MyUtilites
{
	public static class E
	{
		public const object NOTHING = null;
	}

	class MissingEventManagerExeption : Exception
	{
		public MissingEventManagerExeption() :
			base("EventManager should be added to the LoadingScene before to start using it")
		{ }
	}

	class UnityEventObject : UnityEvent<object>
	{

	}

	[DefaultExecutionOrder(-200)]
	public class EventManager : MonoBehaviour
	{
		private static EventManager _instance = null;

		private Dictionary<string, UnityEventObject> _events;

		public static EventManager Instance
		{
			get
			{
				if (_instance == null)
				{
					throw new MissingEventManagerExeption();
				}

				return _instance;
			}

			private set
			{
				_instance = value;
			}
		}

		public static void StartListening(string eventName, UnityAction<object> listener)
		{
			UnityEventObject e = null;
			if (Instance._events.TryGetValue(eventName, out e))
			{
				e.AddListener(listener);
			}
			else
			{
				e = new UnityEventObject();
				e.AddListener(listener);
				Instance._events.Add(eventName, e);
			}
		}

		public static void StopListening(string eventName, UnityAction<object> listener)
		{
			if (_instance == null)
				return;

			UnityEventObject thisEvent = null;

			if (Instance._events.TryGetValue(eventName, out thisEvent))
			{
				thisEvent.RemoveListener(listener);
			}
		}

		public static void EmitEvent(string eventName, object data = E.NOTHING)
		{
			UnityEventObject thisEvent = null;

			if (Instance._events.TryGetValue(eventName, out thisEvent))
			{
				thisEvent.Invoke(data);
			}
		}

		private void Awake()
		{
			if (_instance == null)
			{
				_instance = this;
				DontDestroyOnLoad(gameObject);

				_events = new Dictionary<string, UnityEventObject>();
			}
			else
			{
				DestroyImmediate(gameObject);
			}
		}
	}
}