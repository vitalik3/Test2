using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ShowNormals : MonoBehaviour
{

     private MeshFilter meshFilter;

     // Use this for initialization
     void Start( )
     {
          meshFilter = GetComponent<MeshFilter>( );
     }


     void OnDrawGizmos( )
     {

          //{
          //     for ( int i = 0; i < meshFilter.sharedMesh.vertices.Length; i++ )
          //     {
          //          UnityEditor.Handles.matrix = meshFilter.transform.localToWorldMatrix;
          //          Vector3 pos = meshFilter.sharedMesh.vertices [ i ];
          //          Vector3 norm = meshFilter.sharedMesh.normals [ i ];

          //          UnityEditor.Handles.Label( pos + norm * 0.1f, i.ToString( ) );

          //          UnityEditor.Handles.DrawLine( pos, pos + norm );
          //     }

          //}
     }
}
