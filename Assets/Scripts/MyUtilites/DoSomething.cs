using System.Collections;
using System.Collections.Generic;
using RayFire;
using UnityEngine;
using UnityEngine.Rendering;

public class DoSomething : MonoBehaviour
{
   [Tooltip("root object for replace meshCollider to SphereCollider")] [SerializeField]
   private Transform[] _objectsRoot;

   [SerializeField] private int _amountShatterFirstLevel;
   [SerializeField] private int _amountShatterSecondLevel;
   [SerializeField] private GameObject _rootToShatter;

   private RayfireShatter _shatter;
   private Dictionary<RayfireShatter, List<GameObject>> _dictionary = new Dictionary<RayfireShatter, List<GameObject>>();

   [ContextMenu("ReplaceConvexToSpher")]
   public void ReplaceConvexToSpher()
   {
      foreach (var root in _objectsRoot)
      {
         for (int i = 0; i < root.childCount; i++)
         {
            var obj = root.GetChild(i);
            if (obj.gameObject.GetComponent<Collider>())
            {
               DestroyImmediate(obj.gameObject.GetComponent<Collider>());
            }

            var rend = obj.gameObject.GetComponent<Renderer>();
            rend.receiveShadows = false;
            rend.shadowCastingMode = ShadowCastingMode.Off;
            rend.lightProbeUsage = LightProbeUsage.Off;
            rend.reflectionProbeUsage = ReflectionProbeUsage.Off;

            obj.gameObject.AddComponent<SphereCollider>();
         }
      }
   }


   [ContextMenu("DoFragmentRoot")]
   public void DoShatterDeepth()
   {
      DoFragment(_rootToShatter);
   }

   private void DoFragment(GameObject rootShatter)
   {
      _shatter = rootShatter.AddComponent<RayfireShatter>();

      _shatter.type = FragType.Voronoi;
      _shatter.voronoi.amount = _amountShatterFirstLevel;

      _shatter.Fragment();

      var child = _shatter.fragmentsAll;
      CreateRootRB(_shatter.rootChildList[0].gameObject);
      
      var rootGenerate = _shatter.rootChildList[0].gameObject.GetComponent<RayfireRigid>();
      rootGenerate.simulationType = SimType.Dynamic;
      rootGenerate.objectType = ObjectType.ConnectedCluster;
      rootGenerate.demolitionType = DemolitionType.Runtime;
      rootGenerate.limitations.solidity = 10f;
      rootGenerate.gameObject.layer = LayerMask.NameToLayer(GameTags.PICK_MASK);
      
      rootGenerate.activation.byConnectivity = true;
      var connectivity = rootGenerate.gameObject.AddComponent<RayfireConnectivity>();
      connectivity.demolishable = true;
      var unyielding = rootGenerate.gameObject.AddComponent<RayfireUnyielding>();
      unyielding.size = Vector3.one * .1f; 

      foreach (var go in child)
      {
         _shatter = go.AddComponent<RayfireShatter>();

         _shatter.type = FragType.Voronoi;
         _shatter.voronoi.amount = _amountShatterSecondLevel;

         _shatter.Fragment();

         CreateRootRB(_shatter.gameObject, _shatter.rootChildList[0].gameObject);
         CreateRB(_shatter.rootChildList[0].gameObject);

         var child2 = _shatter.fragmentsAll;
         foreach (var lastObj in child2)
         {
            CreateLastRB(lastObj);
         }
      }
   }

   private void CreateRootRB(GameObject go, GameObject refDemolition = null)
   {
      var rf = go.AddComponent<RayfireRigid>();

      rf.initialization = RayfireRigid.InitType.AtStart;
      rf.simulationType = SimType.Dynamic;
      rf.objectType = ObjectType.Mesh;

      if (refDemolition != null)
      {
         rf.demolitionType = DemolitionType.ReferenceDemolition;
         rf.referenceDemolition.reference = refDemolition;
         rf.referenceDemolition.action = RFReferenceDemolition.ActionType.SetActive;
         refDemolition.SetActive(false);
      }
   }

   private void CreateRB(GameObject go)
   {
      var rf = go.AddComponent<RayfireRigid>();

      rf.initialization = RayfireRigid.InitType.AtStart;
      rf.simulationType = SimType.Dynamic;
      rf.objectType = ObjectType.MeshRoot;
      rf.demolitionType = DemolitionType.None;
   }

   private void CreateLastRB(GameObject go)
   {
      var rf = go.AddComponent<RayfireRigid>();

      rf.initialization = RayfireRigid.InitType.AtStart;
      rf.simulationType = SimType.Dynamic;
      rf.objectType = ObjectType.Mesh;
      rf.demolitionType = DemolitionType.None;

      rf.activation.byVelocity = .1f;

      var fading = rf.fading;
      fading.lifeTime = 4;
      fading.fadeType = FadeType.ScaleDown;
      fading.fadeTime = 3;
   }
}