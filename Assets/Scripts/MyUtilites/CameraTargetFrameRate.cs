﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyUtilites
{
	public class CameraTargetFrameRate : MonoBehaviour
	{
		[SerializeField][Range(1, 240)] private int TARGET_FRAME_RATE = 60;

		private void Awake()
		{
			Application.targetFrameRate = TARGET_FRAME_RATE;
		}

		private void OnValidate()
		{
			Application.targetFrameRate = TARGET_FRAME_RATE;
		}
	}
}

