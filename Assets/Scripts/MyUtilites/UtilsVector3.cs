﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyUtilites.Vector3Extension
{
     public static class UtilsVector3
     {
          public static Vector3 RotateVector3XZ(this Vector3 inputVector3, float angleDegress )
          {
               float angle = angleDegress / Mathf.Rad2Deg;
               Vector3 result = inputVector3;
               result.x = inputVector3.x * Mathf.Cos( angle ) - inputVector3.z * Mathf.Sin( angle );
               result.z = inputVector3.z * Mathf.Cos( angle ) + inputVector3.x * Mathf.Sin( angle );
               return result;
          }

          public static Vector2 RotateVector2XY(this Vector2 inputVector2, float angleDegress)
          {
               var angle_rad = angleDegress * Mathf.Deg2Rad;
               Vector2 result = inputVector2;
               result.x = inputVector2.x * Mathf.Cos(angle_rad) - inputVector2.y * Mathf.Sin(angle_rad);
               result.y = inputVector2.x * Mathf.Sin(angle_rad) + inputVector2.y * Mathf.Cos(angle_rad);
               return result;
          }
     }
}

