﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace MyUtilites
{
     public class FPS : MonoBehaviour
     {
          [SerializeField] private float timeUpdate = .25f;

          private TextMeshProUGUI _tmp;
          private int fps;

          private void Start( )
          {
               _tmp = GetComponent<TextMeshProUGUI>( );
               fps = 0;
               StartCoroutine( UpdateFPS ());
          }

          private IEnumerator UpdateFPS ()
          {
               while (true)
               {
                    fps = ( int ) ( 1f / Time.unscaledDeltaTime );
                    _tmp.text = fps.ToString( );
                    yield return new WaitForSeconds( timeUpdate );
               }
          }

     }
}

