using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameTags
{
    public const string PLAYER = "PLAYER";
    public const string FLOOR = "FLOOR";
    public const string SPHERE = "SPHERE";
    public const string SPHERE_NEUTRAL = "SPHERE_NEUTRAL";
    public const string ENEMY = "ENEMY";
    public const string TOUCH = "TOUCH";
    public const string GROUND = "GROUND";
    public const string TRIGGER_HASH_ITEM = "TRIGGER_HASH_ITEM";
    public const string ONLY_PLAYER = "ONLY_PLAYER";
    public const string PLAYER_ITEM_COLLECTOR = "PLAYER_ITEM_COLLECTOR";
    public const string IGNORE_PLAYER = "IGNORE_PLAYER";
    public const string OBSTACLE = "OBSTACLE";
    public const string STONE_PICKED = "STONE_PICKED";
    public const string PICK_MASK = "PICK_MASK";
    public const string BASE_ANCHOR = "BASE_ANCHOR";
    
}
