using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites.Saves;
using UnityEngine;

public class PalitraManager : MonoBehaviour
{
    [SerializeField] private Material _obstacleMaterial;
    [SerializeField] private Texture [] _textures;

    private void Start()
    {
        UpdatePalitra(0);
        Saves.SelectPalitraTexture.OnChanged += UpdatePalitra;
    }

    private void UpdatePalitra(int indexTexture)
    {
        //_obstacleMaterial.SetTexture("_MainTex", _textures[Saves.SelectPalitraTexture.Value % _textures.Length]);
    }

    private void OnDestroy()
    {
        Saves.SelectPalitraTexture.OnChanged -= UpdatePalitra;
    }
}
