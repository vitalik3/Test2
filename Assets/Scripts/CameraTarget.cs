using System;
using System.Collections;
using System.Collections.Generic;
using MyUtilites;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
   public enum TypeCameraTarget
   {
      FOLLOW,
      LOOK_AT,
   }

   [SerializeField] private TypeCameraTarget _type;

   public TypeCameraTarget Type => _type;


   private void OnEnable()
   {
      EventManager.EmitEvent(GameEventType.INIT_CAMERA_TARGET, this);
   }
}
